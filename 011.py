#
# Galimullin Amir
# 11-406
# 011
#

# coding=utf-8
import os
import shutil

while True:
    s = raw_input(os.path.abspath(os.curdir) + '>').split()
    if len(s) == 0:
        continue
    elif s[0] == 'exit' and len(s) == 1:
        break
    elif s[0] in ('dir', 'ls'):
        for f in os.listdir(os.curdir):
            print f
    elif s[0] == 'supercopy' and len(s) == 3:
        if os.path.exists(s[1]):
            try:
                isinstance(int(s[2]), int) is True
            except ValueError:
                print 'WAT? Write a number!'
                continue
            count = int(s[2])
            k = 0
            while k < count:
                shutil.copy(s[1], '%s-copy%s%s' % (os.path.splitext(s[1])[0],
                                                   str(k).zfill(len(s[2])),
                                                   os.path.splitext((s[1]))[1]))
                k += 1
    elif s[0] == 'cat':
        if s[1] == '>':
            f = open(s[2], 'w')
            string = raw_input()
            while string != ':q':
                f.write(string + '\n')
                string = raw_input()
            f.close()
        else:
            f = open(s[1])
            for l in f:
                if l == '':
                    print l.strip('\n')
            f.close()
    elif s[0] == 'rm' and len(s) == 2:
        if os.path.exists(s[1]):
            os.remove(s[1])
        else:
            print 'File %s not found' % s[1]
    elif s[0] == 'cd' and len(s) == 2:
        if os.path.exists(s[1]):
            os.chdir(s[1])
        else:
            print 'No such directory'
            continue
    elif s[0] == 'append' and len(s) == 2:
        if os.path.exists(s[1]):
            string = raw_input()
            f = open(s[1], 'a')
            while string != ':q':
                f.write(string + '\n')
                string = raw_input()
            f.close()
        else:
            print 'No such file or directory'
    elif s[0] == 'grep' and len(s) == 3:
        index_of_file = 0
        all_files = []
        # проверка на директорию
        if os.path.isdir(s[2]) is True:
            for top, dirs, files in os.walk(s[2]):
                for nm in files:
                    all_files.append(os.path.join(top, nm))
        # если это файл
        else:
            all_files.append(s[2])
        for fle in all_files:
            if os.path.exists(all_files[index_of_file]):
                active_str_i = 0
                quantity_of_str = 0
                list_strings = []
                f = open(all_files[index_of_file])
                # считаем количество линий в файле и добавляем каждую в лист
                for line in f:
                    quantity_of_str += 1
                    list_strings.append(line.strip('\n'))
                f.close()
                f = open(all_files[index_of_file])
                for line in f:
                    active_str_i += 1
                    if s[1] in line and 2 < active_str_i < quantity_of_str - 1:
                        print '\n'+all_files[index_of_file], 'line №%s' % (str(active_str_i))
                        print 'before: %s, %s; after: %s, %s' % (
                                list_strings[active_str_i - 3], list_strings[active_str_i - 2],
                                list_strings[active_str_i], list_strings[active_str_i + 1]+'\n')
                f.close()
                index_of_file += 1
            else:
                print 'No such file or directory'
    else:
        print 'WAT?'
