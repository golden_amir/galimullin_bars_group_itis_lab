# coding=utf-8
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

first = (
    ('1', 'friend'), ('2', 'parent'), ('3', 'sibling'), ('4', 'pet'), ('5', 'child'), ('6', 'significant other')
)

second = (
    ('1', 'sports'), ('2', 'artistic'), ('3', 'intellectual'), ('4', 'technical')
)

third = (
    ('1', 'music'), ('2', 'design'), ('3', 'fitness')
)


def get_item_image_path(instance, filename):
    return instance.name + filename


class Item(models.Model):
    class Meta:
        db_table = "items"
        verbose_name_plural = u'Items'
        unique_together = ['name', 'description']

    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=100, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    likes = models.IntegerField(default=0)
    image = models.ImageField(null=True, upload_to=get_item_image_path)

    first = models.CharField(max_length=6, choices=first)
    second = models.CharField(max_length=4, choices=second)
    third = models.CharField(max_length=3, choices=third)

    id = models.CharField(max_length=50, editable=False, unique=True, primary_key=True)
    favorite = models.ManyToManyField(User, related_name="favorites")

    def __unicode__(self):
        return "%s : %s - %s likes" % (self.description, self.name, self.likes)


class Category(models.Model):
    class Meta:
        db_table = "categories"
        verbose_name_plural = u'Категории'

    name = models.CharField(u"Name", max_length=50)
    parent = models.ForeignKey("self", verbose_name=u"Parent", blank=True, null=True)

    show_all_products = models.BooleanField(u"Show all products", default=True)

    items = models.ManyToManyField(Item, verbose_name=u"Items", blank=True, related_name="categories")
    description = models.TextField(u"Description", blank=True)
    image = models.ImageField(u"Image", upload_to="images", blank=True, null=True)

    product_rows = models.IntegerField(u"Product rows", default=3)
    product_cols = models.IntegerField(u"Product cols", default=3)
    category_cols = models.IntegerField(u"Category cols", default=3)

    id = models.CharField(max_length=50, editable=False, unique=True, primary_key=True)
