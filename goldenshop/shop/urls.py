from django.conf.urls import url

from shop.views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^items$', show_all, name='items')
]
