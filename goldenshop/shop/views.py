from django.shortcuts import render

from shop.models import Item, Category
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect as redirect
from django.shortcuts import render


# Create your views here.
def login(request):
    if request.user.is_authenticated():
        return redirect(reverse("shop:index"))

    if request.method == "GET":
        context = {}
        if "next" in request.GET:
            context["next"] = "?next=" + request.GET["next"]
        return render(request, "login.html", context)

    elif request.method == "POST":
        user = authenticate(
            username=request.POST["username"],
            password=request.POST["password"]
        )
        if user is not None:
            auth_login(request, user)
            if "next" in request.GET:
                return redirect(request.GET["next"])
            else:
                return redirect(reverse("shop:index"))
        else:
            return redirect(reverse("login"))
    else:
        return HttpResponse("405")


@login_required(login_url=reverse_lazy("login"))
def index(request):
    items = request.user.favorites.all()

    return render(request, "index.html",
                  {"username": request.user.username,
                   "items": items})


@login_required(login_url=reverse_lazy("login"))
def show_all(request):
    s = "All items!<br/><a href=\"%s\">Back to Index</a><br><br>" % reverse("shop:index")

    items = Item.objects.all()

    s += "".join(map(lambda x: "<li>%s</li>" % x, items))

    return HttpResponse(s)


def logout(request):
    auth_logout(request)
    return redirect(reverse("login"))
