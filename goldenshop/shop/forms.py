from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from shop.models import Item


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ["name", "description", "price"]


# class ItemForm(forms.Form):
#     