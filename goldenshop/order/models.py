from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models


class Order(models.Model):
    number = models.CharField(max_length=30)
    user = models.ForeignKey(User, verbose_name=u"User", blank=True, null=True)
    session = models.CharField(u"Session", blank=True, max_length=100)

    created = models.DateTimeField(u"Created", auto_now_add=True)

    price = models.FloatField(u"Price", default=0.0)
    tax = models.FloatField(u"Tax", default=0.0)

    customer_firstname = models.CharField(u"firstname", max_length=50)
    customer_lastname = models.CharField(u"lastname", max_length=50)
    customer_email = models.CharField(u"email", max_length=75)

    shipping_address = models.CharField(max_length=100)

    shipping_method = models.CharField(max_length=50, verbose_name=u"Shipping Method", blank=True, null=True)
    shipping_price = models.FloatField(u"Shipping Price", default=0.0)
    shipping_tax = models.FloatField(u"Shipping Tax", default=0.0)

    payment_method = models.CharField(max_length=50, verbose_name=u"Payment Method", blank=True, null=True)
    payment_price = models.FloatField(u"Payment Price", default=0.0)
    payment_tax = models.FloatField(u"Payment Tax", default=0.0)

    account_number = models.CharField(u"Account number", blank=True, max_length=30)
    bank_identification_code = models.CharField(u"Bank identication code", blank=True, max_length=30)
    bank_name = models.CharField(u"Bank name", blank=True, max_length=100)
    depositor = models.CharField(u"Depositor", blank=True, max_length=100)

    message = models.TextField(u"Message", blank=True)
    pay_link = models.TextField(u"pay_link", blank=True)

    id = models.CharField(max_length=50, editable=False, unique=True, primary_key=True)
    requested_delivery_date = models.DateTimeField(u"Delivery Date", null=True, blank=True)
