#
# Galimullin Amir
# 11-406
# 012
#

# coding=utf-8
import os

class SuperFile:
    def __init__(self, path):
        self.path = path
        if os.path.exists(path) is False:
            fl = open(path, 'w')
            fl.close()

    def println(self, s):
        finish_writer = open(self.path, 'a')
        finish_writer.write(s)
        finish_writer.close()

    def __add__(self, file2):
        reader1 = open(self.path)
        content1 = reader1.read()
        reader1.close()
        reader2 = open(file2.path)
        content2 = reader2.read()
        reader2.close()
        writer = open(self.path + file2.path, 'w')
        writer.write(content1)
        writer.write(content2)
        writer.close()

    def __mul__(self, n):
        reader = open(self.path)
        content = reader.read()
        reader.close()
        writer = open(self.path + 'mult' + str(n), 'w')
        writer.write(content*n)
        writer.close()
