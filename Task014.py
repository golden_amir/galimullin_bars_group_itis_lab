#
# Galimullin Amir
# 11-406
# 014
#

numbers = [
    {'symbol': 'M', 'value': 1000},
    {'symbol': 'D', 'value': 500},
    {'symbol': 'C', 'value': 100},
    {'symbol': 'L', 'value': 50},
    {'symbol': 'X', 'value': 10},
    {'symbol': 'V', 'value': 5},
    {'symbol': 'I', 'value': 1},
]


def roman_to_arabic(number):
    index_by_letter = {}
    for index in xrange(len(numbers)):
        index_by_letter[numbers[index]['symbol']] = index
    res = 0
    previous_value = None
    for symbol in reversed(number):
        index = index_by_letter[symbol]
        value = numbers[index]['value']
        if (previous_value is None) or (previous_value <= value):
            res += value
        else:
            res -= value
        previous_value = value
    return res


print roman_to_arabic('XVI')
