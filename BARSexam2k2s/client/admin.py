from django.contrib import admin

# Register your models here.
from client.models import *

admin.site.register(UserProfile)
admin.site.register(Application)