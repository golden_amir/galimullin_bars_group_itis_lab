from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

status = (('new', 'new'), ('in progress', 'in progress'),
          ('review', 'review'), ('done', 'done'))


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    email = models.EmailField()

    def __unicode__(self):
        return self.user.username

    class Meta:
        verbose_name = 'profile'
        verbose_name_plural = 'profiles'


class Application(models.Model):
    firstname = models.CharField(max_length=20, null=False, blank=False)
    lastname = models.CharField(max_length=20, null=False, blank=False)
    application_author = models.ForeignKey(User, null=False)
    text = models.CharField(max_length=1000)
    status = models.CharField(max_length=10, choices=status, null=False, blank=False, default='new')


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


post_save.connect(create_user_profile, sender=User)
