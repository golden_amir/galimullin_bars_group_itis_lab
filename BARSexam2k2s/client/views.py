from django.contrib.auth.models import User
from django.shortcuts import render_to_response, render
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect as redirect
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth
from django.http import HttpResponseRedirect

from client.forms import MakeApplication
from client.models import UserProfile, Application


def login(request):
    args = {}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            request.session['visit'] = True
            return redirect('/')
        else:
            args['login_error'] = "User does not exist"
            return render_to_response('login.html', args)
    else:
        return render_to_response('login.html', args)


def logout(request):
    auth_logout(request)
    return redirect(reverse('login'))


def registration(request):
    args = {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    if request.POST:
        newuser_form = UserCreationForm(request.POST)
        if newuser_form.is_valid():
            newuser_form.save()
            newuser = auth.authenticate(username=newuser_form.cleaned_data['username'],
                                        password=newuser_form.cleaned_data['password2'])
            auth.login(request, newuser)
            return redirect('/')
        else:
            args['form'] = newuser_form
    return render_to_response('registration.html', args)


@login_required(login_url=reverse_lazy("login"))
def user_profile(request):
    profile = UserProfile.objects.get(user=request.user)
    applications = Application.objects.all()
    args = {'applications': applications,
            'profile': profile}
    args.update(csrf(request))
    return render_to_response("user_profile.html", args)


def application(request):
    # profile = UserProfile.objects.get(user=request.user)
    app_form = MakeApplication
    args = {'applications': Application.objects.all(),
            'form': app_form}
    args.update(csrf(request))
    return render_to_response('application.html', args)


def make_application(request):
    form = MakeApplication(request.POST)
    if form.is_valid():
        writer = open('application.pdf', 'w')
        # writer.write(Application.text)
        writer.close()
        # app = form.save(commit=False)
        # profile = UserProfile.objects.get(user=request.user)
        # profile.application_author = User.objects.get(id=request.user.id)
        form.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
