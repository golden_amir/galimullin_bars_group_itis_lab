# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_application_application_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='firstname',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='application',
            name='lastname',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='status',
            field=models.CharField(default=b'new', max_length=10, choices=[(b'new', b'new'), (b'in progress', b'in progress'), (b'review', b'review'), (b'done', b'done')]),
        ),
    ]
