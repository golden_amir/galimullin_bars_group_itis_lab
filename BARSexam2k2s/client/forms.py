from django.forms import ModelForm

from client.models import *


class MakeApplication(ModelForm):
    class Meta:
        model = Application
        fields = ['firstname', 'lastname', 'text']
