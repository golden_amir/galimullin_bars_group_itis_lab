from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class News(models.Model):
    author = models.OneToOneField(User)
    article = models.CharField(max_length=20)
    text = models.CharField(max_length=1000)
