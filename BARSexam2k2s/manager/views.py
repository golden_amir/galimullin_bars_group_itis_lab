from django.shortcuts import render, render_to_response

# Create your views here.
from django.template.context_processors import csrf

from client.models import Application
from manager.models import News


def application(request):
    # profile = UserProfile.objects.get(user=request.user)
    args = {'applications': Application.objects.all(),
            'news': News.objects.all()}
    args.update(csrf(request))
    return render_to_response('applications.html', args)
