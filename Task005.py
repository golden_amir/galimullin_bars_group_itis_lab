#
# Galimullin Amir
# 11-406
# 005
#


from math import *

n = input("Enter the digit: ")
x = 0
y = 0
str = ''
while x < 2 * n + 1:
    while y < 2 * n + 1:
        if sqrt((x - n) * (x - n) + (y - n) * (y - n)) <= n:
            str += '0'
        else:
            str += '*'
        y += 1
    x += 1
    y = 0
    str += '\n'
print str
