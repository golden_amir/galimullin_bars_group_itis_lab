#
# Galimullin Amir
# 11-406
# 008
#

import math


class Vector2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __add__(self, vector):
        vector1 = Vector2D()
        vector1.x = self.x + vector.x
        vector1.y = self.y + vector.y
        return vector1

    def __sub__(self, vector):
        vector1 = Vector2D()
        vector1.x = self.x - vector.x
        vector1.y = self.y - vector.y
        return vector1

    def __mul__(self, real):
        vector = Vector2D()
        vector.x = self.x * real
        vector.y = self.y * real
        return vector

    def __str__(self):
        return "%s, %s" % (self.x, self.y)

    def len(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def scalar(self, vector):
        return self.x * vector.x + self.y * vector.y

    def __eq__(self, vector):
        if self.x == vector.x and self.y == vector.y:
            return 'equal'
        else:
            return 'not equal'
