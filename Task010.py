#
# Galimullin Amir
# 11-406
# 010
#


def decorator(function_to_decorate):
    def a_wrapper_accepting_arguments(*args):
        for arg in args:
            if not isinstance(arg, int):
                raise TypeError('This has not int type', arg)
        function_to_decorate(args)
    return a_wrapper_accepting_arguments


@decorator
def test(args):
    print args
